package com.example.notekeeper.model

data class CourseInfo (val courseId: String, val title: String) {
    override fun toString(): String {
        return title
    }
}