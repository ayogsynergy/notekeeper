package com.example.notekeeper.model

object Seed {

    fun seedCourses(): HashMap<String, CourseInfo> {
        val courses = HashMap<String, CourseInfo>()
        var course = CourseInfo("comp_algo", "Algorithms")
        courses.set(course.courseId, course)

        course = CourseInfo("comp_os", "Operating Systems")
        courses.set(course.courseId, course)

        course = CourseInfo("comp_arch", "Computer Architecture")
        courses.set(course.courseId, course)

        course = CourseInfo("comp_eth", "Engineering Ethics")
        courses.set(course.courseId, course)

        return courses
    }

    fun seedNotes(courses: HashMap<String, CourseInfo>): ArrayList<NoteInfo>{
        val notes = ArrayList<NoteInfo>()
        var note = NoteInfo(courses.get("comp_algo")!!,
            "Sample note 1",
            "Sample note for Algorithms")
        notes.add(note)

        note = NoteInfo(courses.get("comp_os")!!,
            "Sample note 2",
            "Sample note for Operating Systems")
        notes.add(note)

        note = NoteInfo(courses.get("comp_arch")!!,
            "Sample note 3",
            "Sample note for Computer Architecture")
        notes.add(note)

        note = NoteInfo(courses.get("comp_eth")!!,
            "Sample note 4",
            "Sample note for Engineering Ethics")
        notes.add(note)

        return notes
    }
}