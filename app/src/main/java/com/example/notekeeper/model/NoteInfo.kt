package com.example.notekeeper.model

data class NoteInfo (var course: CourseInfo? = null, var title: String? = null, var text: String? = null){
    override fun toString(): String {
        val formatted = text?: "(Empty Note)"
        if(formatted.length > 50){
            return formatted.substring(0, 50)
        }

        return formatted
    }
}