package com.example.notekeeper.service

import com.example.notekeeper.model.CourseInfo
import com.example.notekeeper.model.NoteInfo
import com.example.notekeeper.model.Seed

object Store {
    var courses: HashMap<String, CourseInfo>
    var notes: ArrayList<NoteInfo>

    init {
        courses = Seed.seedCourses()
        notes = Seed.seedNotes(courses)
    }
}